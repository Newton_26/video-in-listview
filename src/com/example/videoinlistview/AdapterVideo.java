package com.example.videoinlistview;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.net.rtp.AudioStream;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.VideoView;

public class AdapterVideo extends BaseAdapter implements SurfaceHolder.Callback{

	Context mContext;
	int[] n;
	ViewHolder mViewHolder;
	SurfaceView mSurfaceView;
	SurfaceHolder mSurfaceHolder;
	MediaPlayer mPlayer;
	public AdapterVideo(Context c ,int [] a){
		this.n = a;
		mContext = c;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return n.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return n[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	private class ViewHolder{
		
		VideoView mVideoView;
		TextView tv_temp;
		ViewHolder(View view){
//			mSurfaceView = (SurfaceView) view.findViewById(R.id.surfaceView1);
//			mVideoView = (VideoView) view.findViewById(R.id.videoView);
			tv_temp = (TextView) view.findViewById(R.id.tv_temp);
		}
	}
	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = convertView;
		mViewHolder = null;
		
		if(rowView == null){
			
			rowView = mInflater.inflate(R.layout.row_item, null);
			mViewHolder = new ViewHolder(rowView);
			mSurfaceView = (SurfaceView) rowView.findViewById(R.id.surfaceView1);
			rowView.setTag(mViewHolder);
		}else{
			mViewHolder = (ViewHolder) rowView.getTag();
		}
		mPlayer = new MediaPlayer();
		mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mSurfaceHolder = mSurfaceView.getHolder();
		mSurfaceHolder.addCallback(AdapterVideo.this);
		mSurfaceHolder.setFixedSize(400, 240);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//		mPlayer.create(mContext,Uri.parse("http://daily3gp.com/vids/747.3gp"),mSurfaceHolder);
		
		// Attach to when audio file is prepared for playing
		mPlayer.setOnPreparedListener(new OnPreparedListener() {
			
			@Override
			public void onPrepared(MediaPlayer arg0) {
				// TODO Auto-generated method stub
				mPlayer.start();
			}
		});
		try {
			mPlayer.setDataSource("https://dl.dropboxusercontent.com/u/10281242/sample_audio.mp3");

//			mPlayer.setDataSource(getDataSource("http://cdnbakmi.kaltura.com/p/1757082/sp/175708200/flvclipper/entry_id/0_8qqtmq9i/version/0"));
			mPlayer.setDisplay(mSurfaceHolder);
			mPlayer.prepareAsync();
//			playVideo();
//			mPlayer.start();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mViewHolder.tv_temp.setText("Temp");
		return rowView;
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		mPlayer.setDisplay(holder);
		try {
//			mPlayer.prepareAsync();
//			mPlayer.start();
		} catch (IllegalStateException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if(mPlayer == null){
			try {
//				mPlayer.create(mContext,Uri.parse("http://daily3gp.com/vids/747.3gp"));
//				mPlayer.setDisplay(holder);
//				playVideo();
//				mPlayer.prepare();
//				mPlayer.start();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	private String getDataSource(final String path) throws IOException {
		if (!URLUtil.isNetworkUrl(path)) {
			return path;
		} else {
			URL url = new URL(path);
			URLConnection cn = url.openConnection();
			cn.connect();
			InputStream stream = cn.getInputStream();
			if (stream == null)
				throw new RuntimeException("stream is null");
			File temp = File.createTempFile("mediaplayertmp", "dat");
			temp.deleteOnExit();
			String tempPath = temp.getAbsolutePath();
			FileOutputStream out = new FileOutputStream(temp);
			byte buf[] = new byte[128];
			do {
				int numread = stream.read(buf);
				if (numread <= 0)
					break;
				out.write(buf, 0, numread);
			} while (true);
			try {
				stream.close();
			} catch (IOException ex) {
			}
			return tempPath;
		}
	}
	private void playVideo()
    {

        new Thread(new Runnable() 
        {
            public void run() 
            {
                try
                {

                    mPlayer.setDataSource(mContext,Uri.parse("http://cdnbakmi.kaltura.com/p/1757082/sp/175708200/flvclipper/entry_id/0_8qqtmq9i/version/0"));
                    mPlayer.prepareAsync();
                }
                catch (IllegalArgumentException e) 
                {
                    Log.d("admin","Error while playing video");
                    e.printStackTrace();
                } 
                catch (IllegalStateException e) 
                {
                    Log.d("admin","Error1 while playing video");
                    e.printStackTrace();
                    Log.i("error", "tag"+e.getMessage());
                } 
                catch (IOException e) 
                {
                    e.printStackTrace();
                    Log.d("admin","Error while playing video.Please, check your network connection");
                    Log.i("error", "tag"+e.getLocalizedMessage());
                }
            }
        }).start();
    }
}
