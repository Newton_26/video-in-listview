package com.example.videoinlistview;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ListView;

public class MainActivity extends Activity {

	ListView lv_video;
	int[] a = {1,2};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lv_video = (ListView)findViewById(R.id.lv_video);
		
		AdapterVideo mAdapterVideo = new AdapterVideo(MainActivity.this,a);
		lv_video.setAdapter(mAdapterVideo);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
